import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'api_currency.dart';
import 'package:simple_currency_converter/Extensions.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> with SingleTickerProviderStateMixin {
  final realEditText = TextEditingController();
  final pesoEditText = TextEditingController();
  final dollEditText = TextEditingController();
  final euroEditText = TextEditingController();

  static const realBuy = 1.0;
  double pesoBuy = 0.0;
  double dollBuy = 0.0;
  double euroBuy = 0.0;

  @override
  Widget build(BuildContext context) =>
      Scaffold(backgroundColor: Colors.grey[200], appBar: appBar(), body: body());

  AppBar appBar() => AppBar(
    title: Text("Simple Converter"),
    backgroundColor: Colors.green[200],
    centerTitle: true,
  );

  Widget body() => Container(
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bgsmooth.jpg"),
            fit: BoxFit.fill,
          )),
      child: FutureBuilder(
          future: fetchCurrency(),
          builder: (context, result) {
            switch (result.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return caseLoading();
              default:
                if (result.hasError)
                  return caseError();
                else
                  return caseSuccess(result);
            }
          }));

  Widget caseLoading() => Center(child: CircularProgressIndicator());

  Widget caseError() => Center(
      child: ElevatedButton(
          child: Text("Oops ¯\\_(ツ)_/¯\n\nClick to try again",
              style: TextStyle(color: Colors.red, fontSize: 25.0), textAlign: TextAlign.center),
          onPressed: () {
            setState(() {});
          }));

  Widget caseSuccess(AsyncSnapshot<dynamic> result) {
    pesoBuy = result.data["results"]["currencies"]["ARS"]["buy"];
    dollBuy = result.data["results"]["currencies"]["USD"]["buy"];
    euroBuy = result.data["results"]["currencies"]["EUR"]["buy"];

    Widget content = buildContent();
    dollEditText.text = "1";
    dollChanged("1");
    return content;
  }

  Widget buildContent() => SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.all(32.0),
              child: Icon(Icons.attach_money_sharp, size: 128.0, color: Colors.green)),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: buildTextField("Real", "R\$", realEditText, realChanged),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: buildTextField("Peso", "\$", pesoEditText, pesoChanged),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: buildTextField("Dollar", "US\$", dollEditText, dollChanged),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: buildTextField("Euro", "€", euroEditText, euroChanged),
          ),
        ],
      ));

  Widget buildTextField(
      String label, String prefix, TextEditingController field, Function function) =>
      TextField(
        controller: field,
        decoration: InputDecoration(
            labelText: label,
            labelStyle: TextStyle(color: Colors.grey[700]),
            border: OutlineInputBorder(),
            prefixText: buildPrefix(prefix)),
        style: TextStyle(color: Colors.green[900], fontSize: 25.0),
        onChanged: function,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
      );

  String buildPrefix(String prefix) {
    while (prefix.length < 5) {
      prefix += " ";
    }
    return prefix;
  }

  void realChanged(String text) {
    double value = double.parse(text);
    pesoEditText.text = realBuy.to(pesoBuy, value).asString();
    dollEditText.text = realBuy.to(dollBuy, value).asString();
    euroEditText.text = realBuy.to(euroBuy, value).asString();
  }

  void pesoChanged(String text) {
    double value = double.parse(text);
    realEditText.text = pesoBuy.to(realBuy, value).asString();
    dollEditText.text = pesoBuy.to(dollBuy, value).asString();
    euroEditText.text = pesoBuy.to(euroBuy, value).asString();
  }

  void dollChanged(String text) {
    double value = double.parse(text);
    realEditText.text = dollBuy.to(realBuy, value).asString();
    pesoEditText.text = dollBuy.to(pesoBuy, value).asString();
    euroEditText.text = dollBuy.to(euroBuy, value).asString();
  }

  void euroChanged(String text) {
    double value = double.parse(text);
    realEditText.text = euroBuy.to(realBuy, value).asString();
    pesoEditText.text = euroBuy.to(pesoBuy, value).asString();
    dollEditText.text = euroBuy.to(dollBuy, value).asString();
  }
}
