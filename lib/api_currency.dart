import 'dart:convert';
import 'package:http/http.dart';

//Deliberately exposed API key
const key = "e24f8f0e";
const request = "https://api.hgbrasil.com/finance?format=json&key=$key";

Future<Map> fetchCurrency() async => json.decode((await get(request)).body);
