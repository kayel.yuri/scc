
extension Currency on double {

  double to(double targetCurrency, double value) =>
      (value * this) / targetCurrency;

  String asString({int decimals = 2}) => this.toStringAsFixed(decimals);

}